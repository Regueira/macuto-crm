@extends('layouts.app')

@section('content')
<div class="row justify-content-md-center">
  <div class="col-md-10">
    <div class="card">
      <div class="card-header">
        Products
      </div>
      <table class="table">
        <thead class="thead-light">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Stock</th>
            <th scope="col">Serial Number</th>
          </tr>
        </thead>
        <tbody>

      	  @foreach ($products as $item)
      	    <tr>
      	      <th scope="row">{{ $item->id }}</th>
      	      <td>{{ $item->product_name }}</td>
      	      <td>{{ $item->description }}</td>
              <td>{{ $item->stock }}</td>
      	      <td>{{ $item->serial_number }}</td>
      	    </tr>
      	  @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection