@extends('errors::minimal')

@section('title', __('Not Found'))
@section('code', '404')
@section('message', __('Not Found'))
@section('image')
<img src="{{ asset('img/macuto-404.png') }}" alt="Macuto" width="102" height="153">
@endsection
                