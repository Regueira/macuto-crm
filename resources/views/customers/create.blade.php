@extends('layouts.app')

@section('content')

<div class="row justify-content-md-center">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
      	Create Customer
      </div>
      <dir class="p-3">
		  {{ Form::open(array('url' => 'customers')) }}

		    {{ csrf_field() }}

	         {{ Form::label('customer_name', 'Name') }}
	         {{ Form::text('customer_name', null, array('class' => 'form-control')) }}

	         {{ Form::label('customer_last_name', 'Last Name') }}
	         {{ Form::text('customer_last_name', null, array('class' => 'form-control')) }}

	         {{ Form::label('phone_number', 'Telephone') }}
	         {{ Form::number('phone_number', null, array('class' => 'form-control')) }}

	         {{ Form::label('mobile', 'Mobile Phone') }}
	         {{ Form::number('mobile', null, array('class' => 'form-control')) }}

	         {{ Form::label('address', 'Address') }}
	         {{ Form::text('address', null, array('class' => 'form-control')) }}

	         {{ Form::label('city', 'City') }}
	         {{ Form::text('city', null, array('class' => 'form-control')) }}

	         {{ Form::label('country', 'Country') }}
	         {{ Form::text('country', null, array('class' => 'form-control')) }}

	         {{ Form::label('postal_code', 'Postal Code') }}
	         {{ Form::number('postal_code', null, array('class' => 'form-control')) }}

	         {{ Form::label('email', 'Email') }}
	         {{ Form::email('email', null, array('class' => 'form-control')) }}

	         {{ Form::label('birthday', 'Age') }}
	         {{ Form::date('birthday', null, array('class' => 'form-control')) }}

	        <br/>
	        {{ Form::submit('Create Customer', array('class' => 'btn btn-primary')) }}

	      {{ Form::close() }}
	   </dir>
    </div>
  </div>
</div>

@endsection