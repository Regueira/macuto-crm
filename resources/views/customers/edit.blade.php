@extends('layouts.app')

@section('content')

<div class="row justify-content-md-center">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
      	Edit Customer
      </div>
	    <form action="/foo/bar" method="POST">
	      @method('PUT')
	      {{ csrf_field() }}
        Name
	      <input type="text" name="name" id="name" value="{{ $name }}">
        Last Name
        Telephone
        Mobile Phone
        Email
        Age
	    </form>
    </div>
  </div>
</div>

@endsection