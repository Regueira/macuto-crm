@extends('layouts.app')

@section('content')
<div class="row justify-content-md-center">
  <div class="col-md-10">
    <a href="{{ URL::to('customers/create') }}" class="btn btn-success" role="button" aria-pressed="true">Create a Customer</a>
    <br/>
    <br/>
    <div class="card">
      <div class="card-header">
        Customers
      </div>
      <table class="table align-middle">
        <thead class="thead-light">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Telephone</th>
            <th scope="col">Mobile Phone</th>
            <th scope="col">E-mail</th>
            <th scope="col">Age</th>
            <th scope="col"></th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
      	  @foreach ($customers as $item)
      	    <tr>
      	      <th scope="row" class="align-middle">{{ $item->id }}</th>
      	      <td class="align-middle">{{ $item->customer_name }}</td>
              <td class="align-middle">{{ $item->customer_last_name }}</td>
      	      <td class="align-middle">{{ $item->phone_number }}</td>
              <td class="align-middle">{{ $item->mobile }}</td>
      	      <td class="align-middle">{{ $item->email }}</td>
              <!-- There are problems with Carbon and laravel 6 -->
              <td class="align-middle">{{ \Carbon\Carbon::parse($item->birthday)->age }} years</td>
              <td><a href="customers/{{ $item->id }}" class="btn btn-primary" role="button" aria-pressed="true"><span class="glyphicon">&#xe000;</span></a></td>
              <td>
                {{ Form::open(array('url' => 'customers/' . $item->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('&#10799;', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
              </td>
      	    </tr>
      	  @endforeach
        </tbody>
      </table>
    </div>
    <br/>
    <div class="container">
        @foreach ($customers as $customer)
            {{ $customer->name }}
        @endforeach
    </div>
    <div class="container d-flex justify-content-center">
    {{ $customers->links() }}
    </div>
  </div>
</div>

@endsection