@extends('layouts.app')

@section('content')

<div class="row justify-content-md-center">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        Customer 
        @foreach ($customers as $customer)
          {{ collect($customers)->first()->id }}
        @endforeach
      </div>
      <dir class="p-3">
        <dl class="dl-horizontal">
          <dt>Name</dt>
          <dd>{{ collect($customers)->first()->customer_name }}</dd>
          <dt>Last Name</dt>
          <dd>{{ collect($customers)->first()->customer_last_name }}</dd>
          <dt>Telephone</dt>
          <dd>{{ collect($customers)->first()->phone_number }}</dd>
          <dt>Mobile Phone</dt>
          <dd>{{ collect($customers)->first()->mobile }}</dd>
          <dt>Email</dt>
          <dd>{{ collect($customers)->first()->email }}</dd>
          <dt>Age</dt>
          <!-- There are problems with Carbon and laravel 6 -->
          <dd>{{ \Carbon\Carbon::parse(collect($customers)->first()->birthday)->age }} years</dd>
        </dl>
      </dir>
    </div>
  </div>
</div>

@endsection