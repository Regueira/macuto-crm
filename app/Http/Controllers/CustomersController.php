<?php

namespace App\Http\Controllers;

use App\Flight;
use App\Customer;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input;
use Illuminate\Support\Facades\Validator;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        $customers = Customer::paginate(15);
        return view('customers.index',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(),[
            'customer_name'        => 'required',
            'customer_last_name'   => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('customers/create#')
                        ->withErrors($validator)
                        ->withInput();
        } else {
            // store
            $customer = new Customer;
            $customer->customer_name           = $request->customer_name;
            $customer->customer_last_name      = $request->customer_last_name;
            $customer->email                   = $request->email;
            $customer->phone_number            = $request->phone_number;
            $customer->mobile                  = $request->mobile;
            $customer->address                 = $request->address;
            $customer->city                    = $request->city;
            $customer->country                 = $request->country;
            $customer->postal_code             = $request->postal_code;
            $customer->birthday                = $request->birthday;
            $customer->identification_document = $request->identification_document;
            $customer->document_type = $request->document_type;
            $customer->save();

            // redirect
            $request->session()->flash('message', 'Successfully created customer!');
            return redirect('customers');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return view('customers.show', ['customers' => Customer::findOrFail($customer)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $customers = Customer::find($customer);

        // show the edit form and pass the nerd
        return view('customers.edit')->with('customer', $customers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        {
            // validate
            $validator = Validator::make($request->all(),[
                'customer_name'        => 'required',
                'customer_last_name'   => 'required'
            ]);

            if ($validator->fails()) {
                return redirect('customers/' . $id . '/edit')
                            ->withErrors($validator)
                            ->withInput();
            } else {
                // store
                $customer_update = Customer::find($customer);
                $customer_update->customer_name           = $request->customer_name;
                $customer_update->customer_last_name      = $request->customer_last_name;
                $customer_update->email                   = $request->email;
                $customer_update->phone_number            = $request->phone_number;
                $customer_update->mobile                  = $request->mobile;
                $customer_update->address                 = $request->address;
                $customer_update->city                    = $request->city;
                $customer_update->country                 = $request->country;
                $customer_update->postal_code             = $request->postal_code;
                $customer_update->birthday                = $request->birthday;
                $customer_update->identification_document = $request->identification_document;
                $customer_update->document_type = $request->document_type;
                $customer_update->save();

                // redirect
                $request->session()->flash('message', 'Successfully updated customer');
                return redirect('customers');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        // delete
        $customer->delete();

        // redirect
        return redirect('customers');
    }
}
